<?php

function WriteHeader() {
    header('Content-Type: application/vnd.google-earth.kml+xml');
    header('Content-Disposition: attachment; filename="pano.kml"');
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    echo "<kml xmlns=\"http://earth.google.com/kml/2.1\">\n";
    echo "<Folder>\n";
    echo "    <name>pano</name>\n";
    echo "    <open>1</open>\n";
}

function WriteFooter() {
    echo "</Folder>\n";
    echo "</kml>";
}

function WritePoint($i, $rayon, $angle) {
 $lat = $_POST['lat'];
 $lon = $_POST['lon'];
 $alt = $_POST['alt'];
 $base_alt = $_POST['base_alt'];
    
 $dlat = ($rayon * cos($angle)) * (90.0 / 10000.0);
 $dlon = ($rayon * sin($angle)) * (90.0 / (10000.0 * cos(deg2rad($lat))));

 $newlat = $lat + $dlat;
 $newlon = $lon + $dlon;

 $range = sqrt( $rayon * $rayon *1000.0*1000.0 + $alt * $alt );
 $tilt = rad2deg( atan($rayon * 1000.0 / $alt) );
 $heading = rad2deg( $angle );

echo "    <Placemark>\n";
echo "        <name>".$i."</name>\n";
echo "        <LookAt>\n";
echo "            <longitude>".$newlon."</longitude>\n";
echo "            <latitude>".$newlat."</latitude>\n";
echo "            <altitude>".$base_alt."</altitude>\n";
echo "            <range>".$range."</range>\n";
echo "            <tilt>".$tilt."</tilt>\n";
echo "            <heading>".$heading."</heading>\n";
echo "            <altitudeMode>absolute</altitudeMode>\n";
echo "        </LookAt>\n";
echo "        <Style>\n";
echo "            <IconStyle>\n";
echo "                <scale>1.1</scale>\n";
echo "                <Icon>\n";
echo "                </Icon>\n";
echo "            </IconStyle>\n";
echo "            <LabelStyle>\n";
echo "                <color>ffffff</color>\n";
echo "                <scale>1.1</scale>\n";
echo "            </LabelStyle>\n";
echo "        </Style>\n";
echo "    </Placemark>\n";
}

function WriteCircle($rayon, $n) {
    echo "<Folder>\n";
    echo "    <name>circle ".$rayon."</name>\n";
    echo "    <open>1</open>\n";

    for ($i = 0; $i < $n; $i++) {
        $angle = $i * (2.0*pi())/$n;
        WritePoint($i + 1, $rayon, $angle);
    }

    //for ($i=0; i < $n ; $i++) {
    //$i = $n - 1;
    //}
    echo "</Folder>\n";
}

if (isset($_POST['lat'])) {
    $lat = $_POST['lat'];
    $lon = $_POST['lon'];
    $alt = $_POST['alt'];
    $f = $alt / 4000.0;
    
    $circles[0][0] = 0;
    $circles[0][1] = 1;
    
    $circles[1][0] = $f * 2.5;
    $circles[1][1] = 6;
    
    $circles[2][0] = $f * 5.0;
    $circles[2][1] = 10;
    
    $circles[3][0] = $f * 10.0;
    $circles[3][1] = 12;
    
    $circles[4][0] = $f * 20.0;
    $circles[4][1] = 16;

    $circles[5][0] = $f * 45.0;
    $circles[5][1] = 20;

    WriteHeader();
    
    for ($i=0; $i < 6 ; $i++) {
        WriteCircle($circles[$i][0], $circles[$i][1]);
    }
    
    WriteFooter();
    
} else {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta content="text/html; charset=ISO-8859-1"
 http-equiv="content-type">
  <title></title>
</head>
<body>
<form method="post" action="panokml.php" name="formkml">
Latitude :&nbsp;<input size="10" name="lat"
 value="48.858"><br>
Longitude :&nbsp;<input size="10" name="lon" value="2.295"><br>
Base Altitude :&nbsp;<input size="10" name="base_alt" value="0"> m<br>
Altitude :&nbsp;<input size="10" name="alt" value="1000"> m<br>
<input type="submit"><br>
</form>
</body>
</html>
<?php
}
?> 
